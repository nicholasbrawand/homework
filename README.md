# Computational Physics Homework/Exercises
This repository is a collection of potential computational physics homework assignements.

## Numerical General Relativity
This assignement was featured in physics today and was designed to introduce 
numerical simulations to students in the context of general relativity! The 
[arxiv paper can be found here](https://arxiv.org/pdf/1803.01678.pdf) and the 
[actual assignment is on github](https://github.com/ckoerber/perihelion-mercury).

## Todo: Numerical solution to single and double pendulum
Students will solve for the equations of motion for a single pendulum using the
small angle approximation and then use numerical simulations to simulate the
single and double pendulum.

## Todo: Molecular Dynamics
I have some friends at the university of Chicago who could provide homeworks 
related to molecular dynamics.

## Todo: Ising Model
Students apply statistical mechanics through this intuitive and ubiquitous model. 
After understanding the 1D analytical solution they will apply the metropolis algorithm to solve for the 2D case. 